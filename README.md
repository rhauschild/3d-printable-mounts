# 3D printable mounts




## Mounts and fixtures:  
  
2022:

<img src="all2022.jpg" width=30% />

<br />

2023:

<img src="./OlympusCameraAdapter/OlympusAdapterRendering.jpg" width=15% />

<br />
<br />
<br />
<br />

## [Camera Adapter Olympus: ./OlympusCameraAdapter](./OlympusCameraAdapter)

<img src="./OlympusCameraAdapter/adapterwithcellphoneliveimage.jpg" width=80% />

This is an adapter for any C-mount camera. We utilize a [ProScope WiFi camera](https://proscopedigital.com/shop/proscope-5mp-microscope-camera-wifi-usb-w-o-battery/) that establishes its own WiFi access point, enabling you to connect to it using your own phone. It's important to note that the printed component lacks threading. The method for attaching the camera involves placing the printed part on the heated hotbed or hotplate to soften it, then proceeding to effortlessly screw the camera onto it. In this manner, the camera creates its own thread during the process.

## [Camera mount: ./blackflyCameraMount ](./blackflyCameraMount)

<img src="./blackflyCameraMount/fullsetupCAD.jpg" width=80% />
<img src="./blackflyCameraMount/fullsetup.jpg" width=80% />

This mount is designed for [FLIR Blackfly cameras](https://www.flir.eu/products/blackfly-s-usb3/?vertical=machine+vision&segment=iis) and can be attached to extruded aluminum profiles measuring 40 mm and 25 mm, as depicted in the images. 

## [Camera mount: ./uEyeCpMount](./uEyeCpMount)

<img src="./uEyeCpMount/mountUSB3uEyeCP.jpg" width=80% />

This mount is designed for [IDS USB3 uEye CP cameras](https://en.ids-imaging.com/store/products/cameras/ids-interface-group/usb-3/ids-family/cp/ids-housing/housed.html) and can be attached to extruded aluminum profiles.  

## [Filter mount: ./filterMount](./filterMount)

<img src="./filterMount/filtermount.jpg" width=80% />
<img src="./filterMount/IRFilterSetup.jpg" width=80% />

This attaches a 2" x 2" x 2mm filter to a Fujinon-CF8ZA-1S lens. In my situation, it's the black glass used for IR imaging: 2" Diameter Optical Cast Plastic IR Longpass Filter, Stock #43-949 (EdmundOptics).
The image above depicts the complete assembly of the uEye camera, camera mount, lens, and filter holder.

## [Filter storage: ./filterStorage](./filterStorage)

<img src="./filterStorage/filterstorage.jpg" width=80% />
<img src="./filterStorage/dummyfilter.jpg" width=30% />
<img src="./filterStorage/filterstoragepart2.jpg" width=80% />

This is an organizational tool for safely storing unused [Cairn opto splitter](https://www.cairn-research.co.uk/product/optosplit-ii/) filters. Additionally, a dummy filter holder is included to maintain the box dust-free. The storage box attaches to an optical table using four 4mm diameter magnets that should be press-fitted into the bottom of the box.


## [Laser scanner mount: ./NovantaScannerMount](./NovantaScannerMount)

<img src="./NovantaScannerMount/scannerMirrorMount.jpg" width=80% />
<img src="./NovantaScannerMount/fullsetup.jpg" width=80% />

This is a mount for a [MOVIA 2-axis scan head](https://novantaphotonics.com/product/movia-2-axis-scan-head/) by Cambridge Technology/ Novata Photonics and a [standard 90° mirror](https://www.thorlabs.com/search/thorsearch.cfm?search=KCB1C/M) to a [metric breadboard](https://www.thorlabs.com/thorproduct.cfm?partnumber=MB1515/M).  


## [Servo tripot mount: ./servoTripodMount](./servoTripodMount)

<img src="./servoTripodMount/servoMount.jpg" width=80% />
<img src="./servoTripodMount/servoMountPic.jpg" width=80% />

The purpose behind this servo mount is to remotely and repeatedly activate mechanical buttons and switches, enabling the automation of tasks that are resistant to other methods of automation. To achieve this, I have designed a highly flexible mount that can be attached to any camera mount, such as a tripod.

There are three large holes designed to accommodate 1/4" tripod inserts. To securely affix these inserts, I recommend placing them on a hotplate, heating them to 240°C (considering that this part is printed using PETG), and then threading them into place. The servo model used is the Reely RS-610WP MG.

For the smaller threaded inserts, I've opted for RS PRO M3 Brass Threaded Inserts with a diameter of 4mm and a depth of 4.78mm. You can find these inserts under RS Stock No. 278-584, available at https://ie.rs-online.com. Additionally, the larger inserts required for tripod mounting are 5/8" to 1/4" inserts.


## [TL switch replacement: ./TLmirrorReplacement](./TLmirrorReplacement)

<img src="./TLmirrorReplacement/TlmirrorOriginal.jpg" width=80% />
<img src="./TLmirrorReplacement/cad.jpg" width=80% />
 
The original Leica TL mirror's manually operated switch is attached to the flip mirror axis using an undersized grub screw. This design flaw makes it susceptible to breaking off. In contrast, the replacement actuator arm is securely press-fitted onto the axis.


## [Eye piece camera Olympus: ./uEyeXCeyePieceforOlympusStereo](./uEyeXCeyePieceforOlympusStereo)

<img src="./uEyeXCeyePieceforOlympusStereo/rendering.jpg" width=80% />
<img src="./uEyeXCeyePieceforOlympusStereo/backsidewithfixationscrewSmall.jpg" width=40% />

This is a mount for an industrial webcam [ueye-xc](https://en.ids-imaging.com/ueye-xc-autofocus-camera.html), allowing it to function as an eyepiece camera on Olympus stereo microscopes. The uEye-XC is a high-resolution color camera with auto-focus capability and a macro lens. It boasts a very high [quantum efficiency (QE)](https://en.ids-imaging.com/IDS/pdfsheet_pdf.php?sku=1008483) and can also be employed for fluorescence imaging. Caution: The camera comes in two versions – UVC (standard webcam) and USB3 Vision.
I am using the [camera with a macro lens](https://en.ids-imaging.com/store/products/cameras/usb-3-ueye-xc-starter-set.html). The lens attaches over the eyepiece (thus, the eyepiece is not removed) and is secured in place with a screw. The red arrows point out two M3 brass threaded inserts. An M3 nylon screw is initially tightly screwed into the knob and then trimmed to the appropriate length. 


<br />
<br />

## Acknowledgment

This project has been made possible in part by grant number 2020-225401 from the Chan Zuckerberg Initiative DAF, an advised fund of Silicon Valley Community Foundation.